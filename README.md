# DST macro template

## Installation

Template for the dst macro is to be found in `/home/hadaq/local/macros/dst`:
```bash
cp -rp /home/hadaq/local/macros/dst ./
cd dst
```
 or better, from a git repository:
 ```bash
git clone https://git.gsi.de/hades-cracovia/software/feb21_dst_template dst
cd dst
```

## Usage

### Update parameters
The main ascii parameter file is `feb21_dst_params.txt`. Inside the `feb21_params` directory there are scripts used to generate it. The generator script is `gen_params.sh`. Call it to redo the param file. Never modify directly `feb21_dst_params.txt` as it may get lost during next generation. Instead add new file starting with index between 10 and 89, and regenerate the param file. E.g.

***Example: Start detector expert needs to overwrite default params (e.g. lookup trable) from root file:***
1. Create file called `20_start_lookup.txt`
2. Put your modified lookup table inside the file.
3. Run `gen_params.sh`
4. Run analysis macro

### Hydra
Initialize default hydra hydra version:
```bash
. /home/hadaq/local/bin/hydralogin
```

### Analysis macro

Compile the macro
```bash
make
```

### Running macro

```bash
./analysisDST hld_file output_Dir [events [first]]
```
where:
* `hld_file` - is path to hld file, the files from eventbuilder can be found inside, most likely `/store/08/01/data/`
* `output_dir` - directory to store unpacked files, must exists
* `events` - how many events, default is 1
* `first` - at which event to start, default is 0

### Custom hydra development
For custom hydra development the best workflow is:
1. Clone the hydra2 repository
1. Create your own brach
1. Make your development
1. Make commit
1. Init your custom hydra
1. Compile and install into custom location
1. Test it with some runtime (analysisDst, etc).
1. If works push to remote server and ask to merge your changes.


#### Detailed description
1. Clone the repository from git.gsi.de. You need to have account there and you must be add as a group member - contact Rafał Lalik (rafal.lalik@uj.edu.pl) if unsure.

```bash
git clone https://git.gsi.de/hades-cracovia/software/hydra2
cd hydra2
```
2. Create your development branch. Most of the time you want to start with the code based on the official hydra release. Thus be sure you are on master branch:
```bash
git checkout master
```
Then create your branch, if you don't have one yet:
```bash
git checkout -b mybranch
```
My recommendation is: if you do start development, call it `start-dev`, if tof, then `tof-dev`, etc...

3. Edit your code in your favourite editor.

4. Commit your changes to the local repository
```bash
git commit -am "Commit message"
```
This is only one of the ways to make commit, if you are not familiar with git, read this manual: https://github.com/git-guides/git-commit

Also, it is good to do commits quite often - how often, it depends. When you finish some certain feature, when you fix a bug, etc.

5. Init your custom hydra, even if you did not install it yet. For trhat you need the init script. The best is take the default one and adjust it:
```bash
cp /home/hadaq/local/bin/hydralogin ../
```
Edit the script and set the desired installation path for your hydra. After that, source it:
```bash
. ../hydralogin
```
6. Compile hydra. You need to provide Makefile and mk scripts:
```bash
cp admin/*.mk admin/Makefile .
```
Adjust Makefile, e.g. put your desired installation path. And compile (you can use many cores as the PC offers 32, also --quiet opton will supress text flood) and install the code.
```bash
make -j20 --quiet && make install
```
7. See part above about macro.

8. When you think that your code is fine, ask to merge it to the future_exp branch. First push the changes to remote repository (rememebr to replace mybranch with your actual branch name):
```bash
git push origin mybranch
```
    Then goto gitlab page: https://git.gsi.de/hades-cracovia/software/hydra2 and perform merge request.

#### Merge request

1. In the panel on the left click on Merge Requests and in the new page select New merge request
1. In the Source branch group choose your branch you want to merge
1. In the Target branch group, be sure that selected repository is hades-cracovia/software/hydra2 and select future_exp as the target branch.
1. In the next page, give the request some name, and write description of all changes. You can also assign one of the maintainers (Rafal or Jörn) to verify the request. You may not want to check "Delete source branch..." as yoy still may want to work in it, and you can remove it always anyway. Probably you do not want to squash, unless you know what you do.
1. When everything is ready, Submit the request. It will be verified and accepted or you receive a message with request tro add extra improvements. After merge request is approved, you may still continue to work in your branch and create new merge request after you are ready with it.


### What next...
So now you do your custom development. I give you some practical examples.

***Example 1***
You are on `future_exp` and want to start new development in the new branch. Before you create the new branch, be sure you have the most recent version of the code. The easiest way is to call
```bash
git pull
```
to update your local repository with most recent changes.

***Example 2***
You have your branch mybranch where you actively develop something. In the meantime in the future_exp someone commited changeswhich you really really need in your code as well, like it fixes nasty bug. Checkout to future_exp and pull the changes (see example 1):
 ```bash
git checkout future_exp
git pull
```
then checkout again to your branch and merge the changes to your branch:
```bash
gut checkout mybranch
git merge future_exp
```
What more, you can do the same withy any other branch. For example there is never master branch version )official hydra) which has important feature for you - merge it! The ecal or tof or other expert commited usefull thig - merge it. In the end, all these changes will met together in future_exp after all the branches are merged.
But please do not merge to future_exp directly, leave the decision to experts who maintain this special branch.
