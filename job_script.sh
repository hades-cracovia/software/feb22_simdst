#!/bin/bash

date

echo "inputs:"
echo " pattern=$pattern"
echo " odir=$odir"

if [[ -n ${SLURM_ARRAY_TASK_ID} ]]; then
    echo "ARRAY: ${SLURM_ARRAY_TASK_ID} in ${pattern}"
    line=$(sed -n $((${SLURM_ARRAY_TASK_ID}+1))p ${pattern})
    IFS=' ' read -ra linearr <<< "$line"
    file=${linearr[0]}
    sodir=${linearr[1]}
else
    echo "NO ARRAY"
    file=$pattern
fi

[ -e job_profile.sh ] && . job_profile.sh

mkdir -p $odir/$sodir
odir=$(readlink -e $odir/$sodir)

#file=$(readlink -e $file)

echo file=$file
echo events=$events
echo odir=$odir

crashfile=$odir/crash_await_${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.txt
echo "$file $sodir"> $crashfile

root -b -q

date

if [ -z ${SLURM_TMPDIR+x} ]; then
    echo "SLURM_TMPDIR is unset"
    dsttmp=$(mktemp -d)
else
    echo "SLURM_TMPDIR is set to '$SLURM_TMPDIR'"
    dsttmp=$(mktemp /tmp/dst.XXXXXXXXXXXXXXXX -d)
fi

echo ./analysisDST $file ${dsttmp} $events
time ./analysisDST $file ${dsttmp} $events

mv ${dsttmp}/$(basename $file .root)*.root $odir -v
mv ${dsttmp}/qa/* $odir/qa -v
rm -rf ${dsttmp}
rm -v $crashfile
