#!/bin/bash

ofile=04_fwdet_lookup_frpc.txt

cat <<EOF > $ofile
##############################################################################
# Lookup table for the TRB3 unpacker of the FwDet RPC detector
# Format:
# trbnet-address  channel  module  layer  strip  lrside
##############################################################################
[FRpcTrb3Lookup]
// Parameter Context: FRpcTrb3LookupProduction
//----------------------------------------------------------------------------
EOF

cat lookup/frpc_lay{0,1,2,3}.txt >> $ofile

cat <<EOF >> $ofile
##############################################################################
EOF
